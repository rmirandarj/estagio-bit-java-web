package com.b2w.estagiobit.model;

public class CalculaBonus {
	
	public double calcularBonus(Funcionario funcionario) {
		double fator = funcionario.getFator();
		
		if(funcionario.getSalario() * funcionario.getFator() > 10000.00) {
			return 10000;
		}
		else if (funcionario.getFator() > 3){
			fator = 3;
		}
		
		return funcionario.getSalario() * fator;
	}
	
}
