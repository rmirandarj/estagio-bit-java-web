package com.b2w.estagiobit.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection= "funcionario")
public class Funcionario {
	
	@Id
	private Long id;
	private String nome;
	private double salario;
	private double fator;
	
	public Funcionario(String nome, double salario, double fator) {
		super();
		this.nome = nome;
		this.salario = salario;
		this.fator = fator;
	}

	public String getNome() {
		return nome;
	}

	public double getSalario() {
		return salario;
	}

	public double getFator() {
		return fator;
	}

	public Long getId() {
		return id;
	}
	
	  @Override
	  public String toString() {
	    return "Funcionario [id=" + id + ", name=" + this.nome + ", salario=" + this.salario + "]";
	  }
	
	
	
}
