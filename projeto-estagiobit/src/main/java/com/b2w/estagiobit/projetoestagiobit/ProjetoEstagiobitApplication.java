package com.b2w.estagiobit.projetoestagiobit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.b2w.estagiobit.dao.FuncionarioDAO;
import com.b2w.estagiobit.model.Funcionario;

@SpringBootApplication
public class ProjetoEstagiobitApplication implements CommandLineRunner{

	@Autowired
	private FuncionarioDAO dao;

	public static void main(String[] args) {
		SpringApplication.run(ProjetoEstagiobitApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		// save a couple of customers
		dao.save(new Funcionario("Alice", 9000, 3));
		dao.save(new Funcionario("Bob", 11000, 2));

	}
}
