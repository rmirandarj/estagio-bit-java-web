package com.b2w.estagiobit.testes;

import org.junit.jupiter.api.Test;

import com.b2w.estagiobit.model.CalculaBonus;
import com.b2w.estagiobit.model.Funcionario;

import junit.framework.TestCase;

public class CalculaBonusTest extends TestCase{
	
	//RN1 limite salario de 10k
	@Test
	public void testa_salario_acima_limite() {
		Funcionario funcionario = new Funcionario("Teste1", 11000, 5);
		CalculaBonus calculator = new CalculaBonus();
		assertEquals(10000.00, calculator.calcularBonus(funcionario));
	}
	
	//RN2 limite fator de 3.0
	@Test
	public void testa_fator_acima_limite() {
		Funcionario funcionario = new Funcionario("Teste2", 1000, 5);
		CalculaBonus calculator = new CalculaBonus();
		assertEquals(3000.00, calculator.calcularBonus(funcionario));
	}
	
}
