package com.b2w.estagiobit.projetoestagiobit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjetoEstagiobitApplicationTests {

	@Test
	public void contextLoads() {
	}

}
